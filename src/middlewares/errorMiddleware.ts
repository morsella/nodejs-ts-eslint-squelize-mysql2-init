import { Request, Response } from 'express';
import HttpException from '../exceptions/HttpException';
 
export default function errorMiddleware(error: HttpException, request: Request, response: Response): void {
    console.log('ERROR', error);
    const status = error.status || 500;
    const message = error.message || 'Something went wrong';
    response.status(status).json({ message: message});
}
 