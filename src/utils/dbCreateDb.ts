import dotenv from 'dotenv';
import mysql from 'mysql2'; 
dotenv.config();
const dbName = process.env.MYSQL_DATABASE;
const pool = mysql.createPool({
    host: process.env.MYSQL_HOST,
    user     : process.env.MYSQL_USER,
    password : process.env.MYSQL_ROOT_PASSWORD,
    connectionLimit: 10
});
export default async(): Promise<void> => {
        pool.getConnection((err, connection) => {
            if (err) { 
                connection.release(); 
                throw err; 
            }
            console.log("Connected!");
            connection.query("CREATE DATABASE IF NOT EXISTS " + dbName + " CHARACTER SET utf8 COLLATE utf8_general_ci;", (err, result) => {
                if (err) { 
                    connection.release();
                    throw err; 
                }
                console.log("Database created" + result);
                connection.release();
            });
        });
}
