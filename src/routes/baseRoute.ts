import { Router, Request, Response, NextFunction } from 'express';
import Controller from "../interfaces/ControllerInterface";
import HttpException from '../exceptions/HttpException';

class BaseRoute implements Controller {
    path = '/';
    router = Router();
    constructor(){
        this.initRoute();
    }
    private initRoute(): void{
        this.router.get(this.path, this.redirectBase)
    }
    private redirectBase = async(req: Request, res: Response, next: NextFunction): Promise<void> => {
        try{
            // res.redirect('http://google.com');
            // console.log('env', process.env.PORT);
            res.status(200).json({
                message: 'fetched'
            });
        }
        catch(err){
            const message = err.message || 'Can\'t navigate';
            next(new HttpException(400, message));
        }

    }
}

export default BaseRoute;