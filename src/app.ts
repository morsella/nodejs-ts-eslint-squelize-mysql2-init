import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
// import bcrypt from 'bcryptjs';
// import session from 'express-session';
// import cookieParser from 'cookie-parser';
import errorMiddleware from './middlewares/errorMiddleware';
import headersMiddleware from './middlewares/headersMiddleware';
import Controller from './interfaces/ControllerInterface';

class App { 
  app: express.Application;
  constructor(controllers: Controller[]) {
    this.app = express();
 
    // this.connectToTheDatabase();
    this.initMiddlewares();
    this.initHeaders();
    this.initControllers(controllers);
    this.initErrorHandler();
  }

  listen(): void{
    const port  = process.env.PORT;
    // console.log(process.env);
    this.app.listen(port, () => {
      console.log(`App listening on the port ${port}`);
    });
  }
  private initMiddlewares(): void {
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: false })); // x-www-form-urlencoded
    this.app.use(bodyParser.json()) // application/json
  }
  private initHeaders(): void{
    this.app.use(headersMiddleware);
  }
 
  private initControllers(controllers: Controller[]): void {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
  private initErrorHandler(): void {
    this.app.use(errorMiddleware);
  }
}
 
export default App;


